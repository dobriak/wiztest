#!/bin/bash

# Enable IAM services, Compute, EKS
# Create service account 
gcloud iam service-accounts create overprivileged --description="Over privileged service accout for demos"  --display-name="overpivileged"

gcloud projects add-iam-policy-binding ${PROJECT_ID} --member="serviceAccount:overprivileged@${PROJECT_ID}.iam.gserviceaccount.com" --role="roles/storage.admin"

# Build container on arm64 mac
#docker-buildx build --platform linux/amd64 -t dobriak/wiztest:0.0.3 . --push

rm -rf .terraform*
terraform init
terraform plan -out jnwiz.out
terraform apply jnwiz.out
./show-instructions.sh > instructions.txt

gcloud container clusters get-credentials jnwiz-gke --zone=us-central1-c --project=${PROJECT_ID}

# WARNING
kubectl create clusterrolebinding permissive-binding --clusterrole=cluster-admin --user=admin  --user=kubelet --group=system:serviceaccounts

export MONGODB_URI=mongodb://wizadmin:${TF_VAR_mongodb_pwd}@$(terraform output --raw mongovm_name).$(terraform output --raw zone).c.$(terraform output --raw project).internal:27017

# Create wiztest namespace and deploy the app in there
kubectl apply -f kubernetes/namespace.yaml 

yq 'with( .spec.template.spec.containers[0]; .env[0].value = env(MONGODB_URI) | .env[1].value = env(SECRET_KEY))' kubernetes/deployment.yaml | kubectl apply -f -

kubectl -n wiztest apply -f kubernetes/service.yaml

echo "Cancel when you get an external IP"
kubectl -n wiztest get svc --watch

open http://$(kubectl -n wiztest get svc wiztest-lb -o yaml | yq '.status.loadBalancer.ingress[].ip')

#gsutil ls gs://jnwiz-bup/mongobackups/
#echo http://jnwiz-bup.storage.googleapis.com/mongobackups/$TS/go-mongodb/user.metadata.json
