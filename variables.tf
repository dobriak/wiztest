variable "gcp_project" {
  default = "wiztest-424312"
}

variable "gcp_region" {
  default = "us-central1"
}

variable "gcp_zone" {
  default = "us-central1-a"
}

variable "prefix" {
  default = "jnwiz"
}

variable "shared_svc_account_email" {
  default  = ""
  nullable = false
}

variable "mongodb_pwd" {
  default  = ""
  nullable = false
}