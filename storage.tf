resource "google_storage_bucket" "wizbucket" {
  name          = "${var.prefix}-bup"
  location      = "US"
  storage_class = "STANDARD"
  project       = var.gcp_project
  force_destroy = true
  labels = {
    "role" = "mongodb-backups"
  }
}

resource "google_storage_bucket_object" "mongobups" {
  name    = "mongobackups/"
  content = "Empty public directory"
  bucket  = google_storage_bucket.wizbucket.name
}

# resource "google_storage_default_object_access_control" "public_rule" {
#   bucket = google_storage_bucket.wizbucket.name
#   role   = "READER"
#   entity = "allUsers"
# }

# Make bucket public
resource "google_storage_bucket_iam_member" "public_member" {
  provider = google
  bucket   = google_storage_bucket.wizbucket.name
  role     = "roles/storage.objectViewer"
  member   = "allUsers"
}