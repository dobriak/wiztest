resource "google_compute_instance" "mongovm" {
  name         = "${var.prefix}-mongovm"
  machine_type = "e2-standard-4"
  zone         = var.gcp_zone
  tags         = ["${var.prefix}-ntag"]

  boot_disk {
    initialize_params {
      image = "debian-11"
      size  = 40
    }
  }

  labels = {
    mongoversion = "mongodb60"
    osversion    = "debian11"
  }

  metadata = {
    startup-script = local.linux_startup
  }

  network_interface {
    network    = google_compute_network.vpc1.name
    subnetwork = google_compute_subnetwork.sn2.name
  }

  service_account {
    email  = var.shared_svc_account_email
    scopes = ["cloud-platform"]
  }
}

locals {
  linux_startup = templatefile("./mongodb_install.sh", { "pwd" = var.mongodb_pwd })
}

