# GKE cluster
data "google_container_engine_versions" "gke_version" {
  location       = var.gcp_region
  version_prefix = "1.28."
}

resource "google_container_cluster" "gke1" {
  name     = "${var.prefix}-gke"
  location = var.gcp_zone

  remove_default_node_pool = true
  initial_node_count       = 1
  network                  = google_compute_network.vpc1.name
  subnetwork               = google_compute_subnetwork.sn2.name
  enable_l4_ilb_subsetting = true
  deletion_protection      = false
  resource_labels = {
    role = "frontend"
  }
}

# Separately Managed Node Pool
resource "google_container_node_pool" "gke1_nodes" {
  name       = google_container_cluster.gke1.name
  location   = var.gcp_zone
  cluster    = google_container_cluster.gke1.name
  version    = data.google_container_engine_versions.gke_version.release_channel_latest_version["STABLE"]
  node_count = 2

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env  = var.gcp_project
      role = "frontend"
    }

    # preemptible  = true
    machine_type = "e2-medium"
    tags         = ["gke-node"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
    disk_size_gb = 40
    disk_type    = "pd-standard"
  }
}
