#!/bin/bash
#
# Print instructions on how to access infrastructure
#
prefix=$(terraform output --raw prefix)
project=$(terraform output --raw project)
zone=$(terraform output --raw zone)
region=$(terraform output --raw region)

echo "---------Networking---------"
echo "VPC name: ${prefix}-vpc"
echo "Subnets: ${prefix}-sn1 10.10.0.0/16, ${prefix}-sn2 10.11.0.0/16"
echo "Firewall rules: ${prefix}-fw1 allow ssh from IAP, ${prefix}-fw2 allow all internal traffic for source/destination tags ${prefix}-ntag"
echo "Router: ${prefix}-rtr1"
echo "NAT: ${prefix}-nat1"

echo "---------Linux VMs---------"
echo "Accessing your Mongodb VM:"
vn=$(terraform output --raw mongovm_name)
echo "https://console.cloud.google.com/compute/instancesDetail/zones/${zone}/instances/${vn}?project=${project}"
echo "gcloud compute ssh --zone $zone  --tunnel-through-iap $vn"


echo "---------GKE cluster---------"
cluster_name=$(terraform output --raw gke_name)
echo "# Prepare kubectl credentials:"
echo "gcloud container clusters get-credentials $cluster_name --zone=$zone --project=$project"
echo "# Verify kubectl is working:"
echo "kubectl get nodes"

echo "----------Public Storage Bucket-------"
bucket=$(terraform output --raw bucket_name)
bup_dir=$(terraform output --raw mongodb_bups_dir)
echo "# Public access folder URL:"
echo "http://${bucket}.storage.googleapis.com/${bup_dir}"
