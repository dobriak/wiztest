# wiztest
## Flawed-by-design demo web app

### Getting started, prerequisites

I make use of the following tools from a command line
```
terraform, gcloud, yq
```

### Architecture

GCP VPC, private subnet with NAT gateway:
* mongodb VM, outdated debian image and older mongo version, backups to public bucket
* overly privileged service account attached to the VM
* publicly exposed Google cloud drive
* EKS cluster running a simple web application and exposing it to the world via LoadBalancer service
* All containers in said EKS cluster have overly privileged access to the cluster APIs

### Spin it up

All infrastructure is created via terraform.

Mongodb and a cron backup job are installed and configured via simple shell script off of the instance metadata service.

Storage bucket is named `jnwiz-bup` and all backups are stored in `mongobackups` directory in there, in the following format: `gs://jnwiz-bup/mongobackups/<TimeStamp>/<database>/<files>`. 

Public access url is `http://jnwiz-bup.storage.googleapis.com/mongobackups/<TimeStamp>/<database>/<files>`.

For example the user metadata file will be available at `http://jnwiz-bup.storage.googleapis.com/mongobackups/<TimeStamp>/go-mongodb/user.metadata.json`

Backups are performed every 10 minutes.

### Destroy it

`terraform destroy`