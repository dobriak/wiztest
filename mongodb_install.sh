#!/bin/bash
(
apt-get update
apt-get install -y sudo gnupg curl net-tools

# Installing 6.0 (current v-1)
curl -fsSL https://www.mongodb.org/static/pgp/server-6.0.asc | gpg -o /usr/share/keyrings/mongodb-server-6.0.gpg --dearmor
echo "deb [ signed-by=/usr/share/keyrings/mongodb-server-6.0.gpg] http://repo.mongodb.org/apt/debian bullseye/mongodb-org/6.0 main" | tee /etc/apt/sources.list.d/mongodb-org-6.0.list
apt-get update
apt-get install -y mongodb-org
# Do not upgrade mongo
echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-database hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-mongosh hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections
echo "Checking if mongod is running 1"
systemctl is-active mongod.service
sudo systemctl daemon-reload
sudo systemctl start mongod
sleep 60s
echo "Checking if mongod is running 2"
systemctl is-active mongod.service
# Configure mongodb users
cat > /tmp/create_adm.js << EOF
use admin
db.createUser(
  {
    user:"wizadmin",
    pwd:"${pwd}",
    roles:[ { role:"userAdminAnyDatabase", db:"admin"}, "readWriteAnyDatabase" ]
  }
)
db.createUser(
  {
    user:"wizroot",
    pwd:"${pwd}",
    roles:[ { role:"root", db:"admin"} ]
  }
)
EOF
mongosh < /tmp/create_adm.js
systemctl stop mongod
# Enable auth in mongo
sed -i -e 's/\#security\:/security:\n  authorization\: enabled/1' -e 's/bindIp\:\ 127.0.0.1/bindIp\:\ 0.0.0.0/1' /etc/mongod.conf
systemctl daemon-reload
systemctl enable --now mongod
rm /tmp/create_adm.js

# Mongo backup script and crontab
cat > /root/bupmongo.sh << EOF
#!/bin/bash
myts=\$(date +%s)
bupdir=/tmp/backups/\$myts
mkdir -p \$bupdir
mongodump -u wizroot --config /etc/mongocreds.yaml --out \$bupdir
gsutil cp -r \$bupdir gs://jnwiz-bup/mongobackups/
pushd /tmp/backups
tar -zcvf latest.tar.gz \$myts
gsutil cp latest.tar.gz gs://jnwiz-bup/mongobackups/
popd
EOF
chmod +x /root/bupmongo.sh
echo "password: ${pwd}" > /etc/mongocreds.yaml
(crontab -l; echo "*/10 * * * * /root/bupmongo.sh") | sort -u | crontab -
echo "Done."
) 2>&1 | tee -a /tmp/mongodb_install.log
