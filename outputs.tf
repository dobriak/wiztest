output "project" {
  value = var.gcp_project
}
output "zone" {
  value = var.gcp_zone
}

output "region" {
  value = var.gcp_region
}

output "prefix" {
  value = var.prefix
}

output "mongovm_name" {
  value = google_compute_instance.mongovm.name
}
output "mongovm_ip" {
  value = google_compute_instance.mongovm.network_interface.0.network_ip
}

output "gke_name" {
  value = join(" ", google_container_cluster.gke1.*.name)
}
output "gke_endpoint" {
  value = join(" ", google_container_cluster.gke1.*.endpoint)
}

output "gke_pod_range" {
  value = google_container_cluster.gke1.cluster_ipv4_cidr
}

output "bucket_name" {
  value = google_storage_bucket.wizbucket.name
}

output "mongodb_bups_dir" {
  value = google_storage_bucket_object.mongobups.name
}
